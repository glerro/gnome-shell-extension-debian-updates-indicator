# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR 2023 Gianni Lerro {glerro} ~ <glerro@pm.me>
# This file is distributed under the same license as the gnome-shell-extension-debian-updates-indicator package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gnome-shell-extension-debian-updates-indicator 2.4.0\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/glerro/gnome-shell-extension-debian-updates-indicator/issues\n"
"POT-Creation-Date: 2024-09-21 22:00+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: debian-updates-indicator@glerro.pm.me/indicator.js:169
msgid "New in repository"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:172
#: debian-updates-indicator@glerro.pm.me/ui/prefs_packages_status_adw1.ui:42
msgid "Local/Obsolete packages"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:175
msgid "Residual config files"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:178
msgid "Autoremovable"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:182
msgid "Apply updates"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:183 debian-updates-indicator@glerro.pm.me/indicator.js:269
msgid "Check now"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:189
msgid "Settings"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:266
msgid "Checking"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:328
#, javascript-format
msgid "%d update pending"
msgid_plural "%d updates pending"
msgstr[0] ""
msgstr[1] ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:359
msgid "Error"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:363
msgid "No internet"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:366
msgid "Initializing"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:370
msgid "Up to date"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:406 debian-updates-indicator@glerro.pm.me/indicator.js:412
msgid "New Update"
msgid_plural "New Updates"
msgstr[0] ""
msgstr[1] ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:413
#, javascript-format
msgid "There is %d update pending"
msgid_plural "There are %d updates pending"
msgstr[0] ""
msgstr[1] ""

#: debian-updates-indicator@glerro.pm.me/indicator.js:617
msgid "Update now"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/monitors.js:80 debian-updates-indicator@glerro.pm.me/monitors.js:89
#, javascript-format
msgid "Can not connect to %s"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/updateManager.js:641
msgid "Last check: "
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:30
msgid "Basic"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:36
msgid "Check for updates every:"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:37
msgid "Automatic checks, 0 to disable"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:56
msgid "Hours"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:57
msgid "Days"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:58
msgid "Weeks"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:72
msgid "Strip out version numbers"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:77
msgid "Highlight security/important updates"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:86
msgid "Indicator"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:89
msgid "Always show the indicator"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:94
msgid "Show updates count on the indicator"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:99
msgid "Auto-expand updates list"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:100
msgid "If updates count is less than this number (0 to disable)"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:122
msgid "Notifications"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:125
msgid "Send a notification when new updates are available"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:130
msgid "Use transient notifications (auto dismiss)"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:135
msgid "How much information to show on notifications"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:136
msgid "Up to a maximum of 50 package names."
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:140
msgid "Count only"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:141
msgid "New updates names"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:142
msgid "All updates names"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:156
msgid "Enable shortcut"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:161
msgid "Shortcut to toggle the menu"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_basic_adw1.ui:165
msgid "Select a shortcut"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:30
msgid "Advanced"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:36
msgid "Reset values for all commands"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:42
msgid "Reset"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:60
msgid "Update method"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:61
msgid "Method to use to apply the updates."
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:72
msgid "Custom"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:80
msgid "Command to update packages <span size=\"small\">(You can use sudo instead of pkexec for the terminal)</span>"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:86
msgid "Show output on terminal"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:91
msgid "Terminal to use <span size=\"small\">(Ex: xterm -e)</span>"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:103
msgid "Use a custom command to check for updates"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:104
msgid "The default command is \"pkcon refresh\", which uses packagekit with the apt backend. You can define a custom command below, to be executed using pkexec."
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_advanced_adw1.ui:109
msgid "Custom command to check for updates"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_packages_status_adw1.ui:30
msgid "Packages"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_packages_status_adw1.ui:36
msgid "New packages in repository"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_packages_status_adw1.ui:37
msgid "Include an item in the indicator with a list of new packages in the repository."
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_packages_status_adw1.ui:43
msgid "Include an item in the indicator with a list of obsolete or local packages. Obsolete packages are not available in the repository. Local packages are available, but an unavailable version is installed."
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_packages_status_adw1.ui:48
msgid "Residual packages"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_packages_status_adw1.ui:49
msgid "Include an item in the indicator with a list of packages that have residual config files. Run \"sudo apt purge pkgname\" to clean them."
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_packages_status_adw1.ui:54
msgid "Autoremovable packages"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_packages_status_adw1.ui:55
msgid "Include an item in the indicator with a list of autoremovable packages. Run \"sudo apt autoremove\" to remove them."
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_ignore_list_adw1.ui:30
msgid "Ignore"
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_ignore_list_adw1.ui:34
msgid "Packages in this list will be excluded from the updates."
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_ignore_list_adw1.ui:35
msgid "As an example, this is useful for packages that are downgraded manually."
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_add_shortcuts_adw1.ui:36
msgid "Press shortcut keys..."
msgstr ""

#: debian-updates-indicator@glerro.pm.me/ui/prefs_add_shortcuts_adw1.ui:37
msgid "Press Esc to exit or Backspace to clear"
msgstr ""
