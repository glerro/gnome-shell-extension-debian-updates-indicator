<?xml version="1.0" encoding="UTF-8"?>
<!--
 * This file is part of Debian Linux Updates Indicator.
 * https://gitlab.gnome.org/glerro/gnome-shell-extension-debian-updates-indicator
 *
 * org.gnome.shell.extension.debian-updates-indicator.gschema.xml
 *
 * Copyright (c) 2023-2024 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Debian Linux Updates Indicator is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Debian Linux Updates Indicator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Debian Linux Updates Indicator. If not, see <https://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 * Original Author: Fran Glais ~ <https://github.com/franglais125>
 * *****************************************************************************
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023-2024 Gianni Lerro <glerro@pm.me>
-->
<schemalist gettext-domain="gnome-shell-extension-debian-updates-indicator">
  <schema id="org.gnome.shell.extensions.debian-updates-indicator" path="/org/gnome/shell/extensions/debian-updates-indicator/" >
    <key type="i" name="check-interval">
      <default>1</default>
      <summary>Interval between updates check</summary>
      <description>Time to wait between two automatic checks</description>
      <range min="0"/>
    </key>
    <key type="i" name="interval-unit">
      <default>1</default>
      <summary>Unit to use for the interval</summary>
      <description>0:hours, 1:days, 2:weeks</description>
    </key>
    <key type="b" name="always-visible">
      <default>true</default>
      <summary>Indicator is always visble</summary>
      <description>If true, the indicator is always visible, even when non updates are pending</description>
    </key>
    <key type="b" name="show-count">
      <default>true</default>
      <summary>Show updates count on indicator</summary>
      <description>If true, the indicator will display the number of updates pending</description>
    </key>
    <key type="b" name="notify">
      <default>false</default>
      <summary>Send a notification when new updates are available</summary>
      <description>Send a notification when new updates are available</description>
    </key>
    <key type="i" name="verbosity">
      <default>0</default>
      <summary>How much information to show on notifications</summary>
      <description>0:Count only, 1:New updates names, 2:All updates names</description>
    </key>
    <key type="b" name="transient">
      <default>true</default>
      <summary>Use transient notifications (auto dismiss)</summary>
    </key>
    <key type="b" name="use-shortcut">
      <default>false</default>
      <summary>Use a shortcut to open the indicator.</summary>
    </key>
    <key type="as" name="debian-updates-indicator-shortcut">
      <default><![CDATA[['<Super>i']]]></default>
      <summary>Keybinding to open the indicator.</summary>
      <description>If showing, the indicator menu is toggled.</description>
    </key>
    <key type="b" name="use-custom-cmd">
      <default>false</default>
      <summary>Allow checks for updates without a passwrd</summary>
    </key>
    <key type="s" name="check-cmd-custom">
      <default>"pkexec apt update"</default>
      <summary>Custom command to run to check for updates</summary>
      <description>Command to run to check for updates.</description>
    </key>
      <key type="i" name="update-cmd-options">
      <default>0</default>
      <summary>Application to use to update, or custom command</summary>
      <description>0:gnome-software, 1:synaptic-pkexec, 2: gpk-update-viewer, 3: update-manager, 4: custom command</description>
    </key>
    <key type="s" name="update-cmd">
      <default>"pkexec apt upgrade -y"</default>
      <summary>Command to run to update packages.</summary>
      <description>Command to run to update packages.</description>
    </key>
    <key type="b" name="output-on-terminal">
      <default>false</default>
      <summary>Show the output of the upgrade process on a terminal window</summary>
      <description>This is a safer way of upgrading packages.</description>
    </key>
    <key type="s" name="terminal">
      <default>"gnome-terminal -x bash -c"</default>
      <summary>Terminal and options to use</summary>
      <description>Terminal and options to execute a shell for the upgrade process</description>
    </key>
    <key type="b" name="strip-versions">
      <default>true</default>
      <summary>Remove version numbers from checkupdates output</summary>
    </key>
    <key type="b" name="show-critical-updates">
      <default>true</default>
      <summary>Distinguishes security/important updates from regular updates</summary>
    </key>
    <key type="i" name="auto-expand-list">
      <default>0</default>
      <summary>Auto-open list submenu when updates count is lower than this number</summary>
      <description></description>
      <range min="0" max="100"/>
    </key>
    <key type="d" name="last-check-date-double">
      <default>0</default>
      <summary>Date and time of the last time a check for updates was run.</summary>
      <description>This is stored and used on the next boot.</description>
    </key>
    <key type="d" name="last-check-date-automatic-double">
      <default>0</default>
      <summary>Date and time of the last time a check for updates was run automatically.</summary>
      <description>This is stored and used on the next boot. Used for calculating when the next check should take place.</description>
    </key>
    <key type="b" name="new-packages">
      <default>true</default>
      <summary>Show a list of new packages</summary>
      <description>When a new package is found in the repository, it is added to a list.</description>
    </key>
    <key type="b" name="obsolete-packages">
      <default>true</default>
      <summary>Show a list of obsolete packages</summary>
      <description>When an obsolete package is found, it is added to a list.</description>
    </key>
    <key type="b" name="residual-packages">
      <default>true</default>
      <summary>Show a list of residual packages</summary>
      <description>When config files from package are not deleted after removal, the package is added to a list.</description>
    </key>
    <key type="b" name="autoremovable-packages">
      <default>true</default>
      <summary>Show a list of autoremovable packages</summary>
      <description>When a package is no longer required, it is added to a list.</description>
    </key>
    <key type="as" name="ignore-list">
      <default>["dummy"]</default>
      <summary>Packages to ignore list</summary>
      <description>A list of packages to ignore for updates.</description>
    </key>
  </schema>
</schemalist>

